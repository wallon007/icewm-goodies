#!/bin/bash
# -*- mode:sh -*-
#
# This program is based on BobC's, PPC's and icewm team's effort.
#

# localization
TEXTDOMAINDIR=/usr/share/locale
TEXTDOMAIN=icewm-remember-settings

#initial values
focus="-f"
select=0
list=0
loop=1

# check for extra options
while [[ $# -ge 1 ]]
do
    case $1 in
        (-s) select=1 ;;
        (-l) list=1 ;;
    esac
    shift
done

# locate winoptions
if [[ -d $ICEWM_PRIVCFG ]]
then
	winoptions=$ICEWM_PRIVCFG/winoptions
elif [[ -d $XDG_CONFIG_HOME && -d $XDG_CONFIG_HOME/icewm ]]
then
	winoptions=$XDG_CONFIG_HOME/icewm/winoptions
elif [[ -d $HOME/.config/icewm ]]
then
	winoptions=$HOME/.config/icewm/winoptions
elif [[ -d $HOME/.icewm ]]
then
	winoptions=$HOME/.icewm/winoptions
else
	echo "$0: Cannot find your icewm config directory" >&2
	exit 1
fi

#function to call again this program, but having the user click on the specific window
select_window(){
	echo "selecting window"
	# remove temporary file
	rm -f -- "$temp"
	#give select option
	select=1
	focus=
	loop=1
}

#function to add/remove changes
save_changes(){
	# delete all previous data for the program
	#delete geometry
	sed -i -e "/$cls_geo/d" "$winoptions"
	#delete layer
	sed -i -e "/$cls_lay/d" "$winoptions"
	#delete workspace
	sed -i -e "/$cls_wsp/d" "$winoptions"
	
	# add new selected information for the program, reading the temp file
    while IFS= read -r line
	do
		# geometry
		if [[ "$line" == "geometry" ]]
		then
			# add new
			echo "$new_geo" >> "$winoptions"
		fi
		# layer
		if [[ "$line" == "layer" ]]
		then
			# add new
			echo "$new_lay" >> "$winoptions"
		fi
		# workspace
		if [[ "$line" == "workspace" ]]
		then
			# add new
			echo "$new_wsp" >> "$winoptions"
		fi
	done < "$temp"
	
	# remove temporary file
	rm -f -- "$temp"
	
	# let icewm reload the winoptions
	icesh winoptions
	
	#stop select option
	select=0
	
	#if list not selected, stop loop
	if [[ list -eq 0 ]]; then
	loop=0
	fi
}

#launches help window
help_dialog()
{
	yad --image="gtk-dialog-info" --height=250 --width=400 --scroll \
	--title=$"HELP" --class="IceWM-remember-settings" --name="help" --borders=10 --center \
	--form --field=$"Save the <b>size and position</b>, <b>workspace</b> \
and <b>layer</b> of a window using the IceWM-remember-settings app.":LBL '' \
	--field=$"Next time you launch the program, it will remember the \
window properties last saved.":LBL '' \
	--field=$"You can also delete this information unticking all options.":LBL '' \
	--field=$"Use the <b>Select other</b> option to select a different \
window/program to configure.":LBL '' --separator="" --button=gtk-quit:0 --buttons-layout=center
}

#launches list dialog
list_dialog(){
	#List names of all currently open windows (we may have to exclude Conky from the list) - Original version over at: https://pastebin.com/13em3H11
	wmctrl -l|awk '{$3=""; $2=""; $1=""; print $0}' > /tmp/windowlist.txt
	#Window to be exluded from the list:
	#exclude_from_window_list="Conky (dhc"
	grep -v "Conky (" /tmp/windowlist.txt > /tmp/windowlist2.txt; mv /tmp/windowlist2.txt /tmp/windowlist.txt
	# Use Yad to select window to "Remeber":
	selection=$(yad --class="IceWM-remember-settings" --name="list" --title=$"Add/Remove IceWM Window Defaults" \
--width=550 --height=400 --borders=20 --text=$"<b>Select a program. Store it's window properties.</b>" \
--text-align=center --center --separator=" " --list  --column=$"What window configuration you want antiX to remember/forget?" \
--button=gtk-ok:0 --button=gtk-quit:1 < /tmp/windowlist.txt)
	#make the window the user selected the active one:
	wmctrl -R $selection
	# if nothing was selected simply exit
	[ -z "$selection" ] && loop=0 && echo "nothing was selected" && exit
}

main_dialog(){
	
	#restart data
	geo=
	x=
	y=
	left=
	top=
	class=
	layer=
	work=
	appname=
	appclass=
	
	# create temporary file
	temp=$(mktemp)
	
	# obtain window settings
	icesh $focus getGeometry getLayer getWorkspace \
		prop WM_CLASS prop _NET_FRAME_EXTENTS >"$temp"

	# file must have something
	if [[ ! -s $temp ]]
	then
		exit 1
	fi

	# remove punctuation
	sed -i -e 's|[+,]| |g' "$temp"

	# read values from file
	while read a b c d e f g h
	do
		if [[ $a =~ ^0x ]]
		then
			if [[ $b =~ ^WM_CLASS ]]
			then
				class=$d
			elif [[ $b =~ ^_NET_FRAME_EXTENTS ]]
			then
				left=$d
				top=$f
			elif [[ $b =~ ^[0-9]+$ || $b -eq -1 || $c = '"All"' ]]
			then
				work=$b
			elif [[ $b =~ ^[A-Z][a-z] ]]
			then
				layer=$b
			fi
		elif [[ $a =~ ^[0-9] ]]
		then
			geo=$a
			x=$b
			y=$c
		fi
	done <"$temp"
	
	# remove temporary file
	rm -f -- "$temp"
	
	#get more values:
	appclass=${class%.*} #delete last dot
	appname=${appclass%.*} #get window name
	appclass=${appclass#*.} #get window class
	appgeo="${geo}+${x}+${y}"

	# correct geometry
	let x-=$left
	let y-=$top

	cls_geo="${class}.geometry"
	new_geo="${cls_geo}: ${geo}+${x}+${y}"

	cls_lay="${class}.layer"
	new_lay="${cls_lay}: ${layer}"

	cls_wsp="${class}.workspace"
	new_wsp="${cls_wsp}: ${work}"
	
	#recreate temp file for yad script
	temp=$(mktemp)

	# main yad dialog
	yad --title=$"Add/Remove IceWM Window Defaults" --class="IceWM-remember-settings" --name="IceWM-remember-settings" \
		--text=$"Entries shown below are for the <b>$appclass</b> ($appname) window.\n\n\
All options marked will be saved, all unmarked will be deleted.\n\n \
Note: Workspace number shown is the window's current workspace. \n \
	Don't worry that it appears too low.\n\n" \
		--center --borders=20 --checklist --list --separator="" \
		--column=$"Select" --column="Entry" --column=$"Type" --column=$"Value" \
	true "geometry" $"Geometry" "$appgeo" \
	true "layer" $"Layer" "$layer" true $"workspace" "Workspace" "$work" >"$temp" \
		--hide-column=2 --print-column=2 --height=320 \
		--button=gtk-help:"bash -c help_dialog" --button=$"Select other":2 --button=gtk-ok:0 --button=gtk-quit:1 	
	
	##SELECTION MADE##
	exval=$?
		case $exval in
			0) save_changes;;
			2) select_window;;
			1) loop=0 ;;
			252) loop=0 ;;
		esac
	
	# remove temporary file
	rm -f -- "$temp"
}

###SCRIPT STARTS HERE###

#ready help window
export -f help_dialog
select_window
	
while [[ $loop -eq 1 ]]; do
    if [[ $select -eq 1 ]]
	then
		focus=
		main_dialog
	elif [[ $list -eq 1 ]]
	then
		focus="-f"
		list_dialog
		main_dialog
	else
		loop=0
		focus="-f"
		main_dialog
	fi
done

#clean extra yad windows
unset help_dialog
