# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Henry Oquist <henryoquist@nomalm.se>, 2023
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-02-19 16:07+0200\n"
"PO-Revision-Date: 2023-02-19 14:12+0000\n"
"Last-Translator: Henry Oquist <henryoquist@nomalm.se>, 2023\n"
"Language-Team: Swedish (https://www.transifex.com/anticapitalista/teams/10162/sv/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: sv\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: icewm-quick-personal-menu-manager:13
msgid "You are running an IceWM desktop"
msgstr "Du använder ett IceWM-skrivbord"

#: icewm-quick-personal-menu-manager:15 icewm-quick-personal-menu-manager:119
msgid "Warning"
msgstr "Varning"

#: icewm-quick-personal-menu-manager:15
msgid "This script is meant to be run only in an IceWM desktop"
msgstr "Detta skript är endast avsett att köras i ett IceWM-skrivbord"

#: icewm-quick-personal-menu-manager:77 icewm-quick-personal-menu-manager:85
#: icewm-quick-personal-menu-manager:101 icewm-quick-personal-menu-manager:133
#: icewm-quick-personal-menu-manager:153 icewm-quick-personal-menu-manager:218
#: icewm-quick-personal-menu-manager:327
msgid "Personal Menu Ultra Fast Manager"
msgstr "Ultrasnabb hanterare av Personlig  Meny"

#: icewm-quick-personal-menu-manager:77
msgid "Help::TXT"
msgstr "Help::TXT"

#: icewm-quick-personal-menu-manager:77
msgid ""
"What is this?\\nThis utility adds and removes application icons from IceWm's"
" 'personal' list.\\nApplication icons are created from an application's "
".desktop file.\\nWhat are .desktop files?\\nUsually a .desktop file is "
"created during an application's installation process to allow the system "
"easy access to relevant information, such as the app's full name, commands "
"to be executed, icon to be used, where it should be placed in the OS menu, "
"etc.\\n.\\n Buttons:\\n 'ADD ICON' - select, from the list, application you "
"want to add to your 'personal' list and it instantly shows up in the menu or"
" submenu.\\nIf, for some reason, the correct icon for your application is "
"not found, a 'gears' icon will be used, so that you can still click it to "
"access the application.\\nYou can click the 'Advanced' button to manually "
"edit the relevant entry and change the application's icon.\\n'UNDO LAST "
"STEP' - every time an icon is added or removed from the toolbar, A backup "
"file is created. If you click this button, a restore is performed from that "
"backup file, without any confirmation.\\n'REMOVE ICON' - this shows a list "
"of all applications that have icons on your 'personal' list. Double left "
"click any application to remove its icon from the list\\n'ADVANCED' - allows"
" for editing the text configuration file that stores all of the entries in "
"your  'personal' list. Manually editing this file allows users to rearrange "
"the order of the icons and delete or add any icon. A brief explanation about"
" the inner workings of the text configuration file is displayed before the "
"file is opened for edition.\\n Warnings: only manually edit a configuration "
"file if you are sure of what you are doing! Always make a back up copy "
"before editing a configuration file!"
msgstr ""
"Vad är detta?\\nDetta verktyg lägger till och tar bort programikoner från "
"IceWm's 'personliga' lista.\\nProgramikoner skapas av ett programs "
"skrivbordsfil.\\nVad är skrivbordsfiler?\\nVanligen skapas en skrivbordsfil "
"under ett programs installationsprocess för att ge systemet enkel tillgång "
"till relevant information, som programmets hela namn, kommandon som kan "
"utföras, ikon som ska användas, var den ska placeras i OS-menyn, etc.\\n.\\n"
" Knappar:\\n 'LÄGG TILL IKON' - välj, från listan, det program du vill lägga"
" till på 'personlig' lista och det visas genast i menyn eller "
"undermeny.\\nOm, av någon orsak, den korrekta ikonen för ditt program inte "
"kan hittas, kommer en kugghjuls-ikon att användas, så du fortfarande kan "
"klicka på den för att nå programmet.\\nDu kan klicka på 'Avancerat'-knappen "
"för att manuellt redigera relevant post och ändra programmets ikon.\\n'ÅNGRA"
" SISTA STEG' - varje gång en ikon läggs till eller tas bort från "
"verktygsfältet, skapas en backupfil. Om du klickar på denna knapp, kommer en"
" återställning att genomföras från denna backupfil, utan någon "
"bekräftelse.\\n'TA BORT IKON' - detta visar en lista över alla program som "
"har ikoner i din 'personliga' lista. Dubbel-vänsterklicka på vilket program "
"som helst för att ta bort dess ikon från listan\\n'AVANCERAT' - tillåter "
"redigering av textkonfigurations-filen som lagrar alla poster i din  "
"'personliga' lista. Redigering av denna fil manuellt tillåter användare att "
"omarrangera ikonernas ordning och ta bort eller lägga till ikoner. En kort "
"förklaring av textkonfigurations-filens mekanismer visas innan filen visas "
"för redigering.\\n Varning: redigera en konfigurationsfil endast om du vet "
"vad du gör! Gör alltid en backup innan du redigerar en konfigurationsfil!"

#: icewm-quick-personal-menu-manager:85
msgid "Warning::TXT"
msgstr "Varning::TXT"

#: icewm-quick-personal-menu-manager:85
msgid ""
"If you click to continue, the 'personal' configuration file will be opened for manual edition.\\n\n"
"How-to:\\nEach icon is identified by a line starting with 'prog' followed by the application name, icon and the application executable file.\\n Move, edit or delete the entire line referring to each entry.\\nNote: Lines starting with # are comments only and will be ignored.\\nThere can be empty lines.\\nAny changes appear instantly on the menu.\\nYou can undo the last change from UNDO LAST STEP button."
msgstr ""
"Om du klickar på fortsätt, kommer den 'personliga' konfigurationsfilen att öppnas för manuell redigering.\\n\n"
"How-to:\\nVarje ikon identifieras av en rad som börjar med 'prog' följt av programnamnet, ikon och programmets körbara fil.\\n Flytta, redigera eller ta bort hela raden som refererar till varje post.\\nNotera: Rader som börjar med # är bara kommentarer och kommer att ignoreras.\\nDet kan finnas tomma rader.\\nAlla ändringar visas genast i menyn.\\nDu kan ångra den senaste ändringen via ÅNGRA SISTA STEG-knappen."

#: icewm-quick-personal-menu-manager:101
msgid "Double click any Application to remove its icon:"
msgstr "Dubbelklicka på  en Applikation för att ta bort dess ikon:"

#: icewm-quick-personal-menu-manager:101
msgid "Remove"
msgstr "Ta bort"

#: icewm-quick-personal-menu-manager:112
msgid "file has something"
msgstr "filen har något"

#: icewm-quick-personal-menu-manager:117
msgid "file is empty"
msgstr "filen är tom"

#: icewm-quick-personal-menu-manager:119
msgid "No changes were made!\\nTIP: you can always try the Advanced buttton."
msgstr ""
"Inga ändringar gjordes!\\nTIPS: du kan alltid försöka med Avancerat-knappen."

#: icewm-quick-personal-menu-manager:133
msgid "Double click any Application to move its icon:"
msgstr "Dubbelklicka på en Applikation för att flytta dess ikon:"

#: icewm-quick-personal-menu-manager:133
msgid "Move"
msgstr "Flytta"

#: icewm-quick-personal-menu-manager:143
msgid "nothing was selected"
msgstr "inget valdes"

#: icewm-quick-personal-menu-manager:153
#, sh-format
msgid "Choose what do to with $EXEC icon"
msgstr "Välj vad som ska göras med $EXEC ikon"

#: icewm-quick-personal-menu-manager:218
msgid "Add selected app's icon"
msgstr "Lägg till vald apps ikon"

#: icewm-quick-personal-menu-manager:331
msgid "HELP!help:FBTN"
msgstr "HJÄLP!help:FBTN"

#: icewm-quick-personal-menu-manager:332
msgid "ADVANCED!help-hint:FBTN"
msgstr "AVANCERAT!help-hint:FBTN"

#: icewm-quick-personal-menu-manager:333
msgid "ADD ICON!add:FBTN"
msgstr "LÄGG TILL IKON!add:FBTN"

#: icewm-quick-personal-menu-manager:334
msgid "REMOVE ICON!remove:FBTN"
msgstr "TA BORT IKON!remove:FBTN"

#: icewm-quick-personal-menu-manager:335
msgid "UNDO LAST STEP!undo:FBTN"
msgstr "ÅNGRA SISTA STEGET!undo:FBTN"
