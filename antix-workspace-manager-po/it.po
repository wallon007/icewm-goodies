# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Guybrush88 <erpizzo@alice.it>, 2023
# Pierluigi Mario <pierluigimariomail@gmail.com>, 2023
# Dede Carli <dede.carli.drums@gmail.com>, 2023
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-02-28 17:40+0200\n"
"PO-Revision-Date: 2023-02-28 15:48+0000\n"
"Last-Translator: Dede Carli <dede.carli.drums@gmail.com>, 2023\n"
"Language-Team: Italian (https://www.transifex.com/anticapitalista/teams/10162/it/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: it\n"
"Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;\n"

#: antiX-workspace-manager:26
msgid "aWCS antiX workspace name changer"
msgstr "modificatore di nome dello spazio di lavoro aWCS di antiX"

#: antiX-workspace-manager:29
msgid "antiX workspace name changer"
msgstr "modificatore di nome dello spazio di lavoro di antiX"

#: antiX-workspace-manager:33
msgid "Enter the desired names:"
msgstr "Digita i nomi desiderati:"

#: antiX-workspace-manager:51
msgid "Workspace Number"
msgstr "Numero dello spazio di lavoro"

#: antiX-workspace-manager:71 antiX-workspace-manager:90
msgid "Back"
msgstr "Indietro"

#: antiX-workspace-manager:74
msgid "Leave"
msgstr "Esci"

#: antiX-workspace-manager:76
msgid "Click Apply when desired names of workspaces have been entered."
msgstr ""
"Clicca su Applica quando i nomi desiderati degli spazi di lavoro sono stati "
"immessi."

#: antiX-workspace-manager:77 antiX-workspace-manager:89
msgid "Apply"
msgstr "Applica"

#: antiX-workspace-manager:110
msgid "aWCS antiX workspace count switcher"
msgstr "scambiatore di conteggio dello spazio di lavoro aWCS di antiX"

#: antiX-workspace-manager:113
msgid "antiX workspace count switcher"
msgstr "scambiatore di conteggio dello spazio di lavoro di antiX"

#: antiX-workspace-manager:117
msgid "Set the desired number of desktops:"
msgstr "Imposta il numero desiderato di scrivanie:"

#: antiX-workspace-manager:119
msgid ""
"Use mouse wheel or click on arrows. You can also enter a number by keyboard "
"followed by enter key. The change takes place immediately."
msgstr ""
"Usa la rotellina del mouse o clicca sulle frecce. Puoi anche inserire un "
"numero tramite la tastiera seguito dal tasto Invio. Il cambiamento avviene "
"immediatamente."

#: antiX-workspace-manager:126
msgid "To change Workspace names click Change Names."
msgstr "Per cambiare i nomi degli spazi di lavoro clicca su Modifica nomi."

#: antiX-workspace-manager:127 antiX-workspace-manager:139
msgid "Change Names"
msgstr "Cambia nomi"

#: antiX-workspace-manager:129
msgid "Click Done when desired number of workspaces was set."
msgstr ""
"Clicca Fatto quando il numero desiderato di spazi di lavoro è stato "
"impostato."

#: antiX-workspace-manager:130
msgid "Done"
msgstr "Fatto"
