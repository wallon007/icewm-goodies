��          �      l      �     �     �       !   #  .   E  0   t     �  	   �     �  D   �             7   '     _     x     �      �     �     �     �  h  �  *   M  !   x  ?   �  V   �  v   1  p   �          4     H  �   _  1   �     $	  g   5	  +   �	     �	     �	  :    
      ;
     \
      v
     
                                                                         	                    ADD ICON!add:FBTN ADVANCED!help-hint:FBTN Add selected app's icon Choose what do to with $EXEC icon Double click any Application to move its icon: Double click any Application to remove its icon: HELP!help:FBTN Help::TXT Move No changes were made!\nTIP: you can always try the Advanced buttton. REMOVE ICON!remove:FBTN Remove This script is meant to be run only in an IceWM desktop UNDO LAST STEP!undo:FBTN Warning Warning::TXT You are running an IceWM desktop file has something file is empty nothing was selected Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-19 14:12+0000
Last-Translator: Tymofii Lytvynenko <till.svit@gmail.com>, 2023
Language-Team: Ukrainian (https://app.transifex.com/anticapitalista/teams/10162/uk/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: uk
Plural-Forms: nplurals=4; plural=(n % 1 == 0 && n % 10 == 1 && n % 100 != 11 ? 0 : n % 1 == 0 && n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 12 || n % 100 > 14) ? 1 : n % 1 == 0 && (n % 10 ==0 || (n % 10 >=5 && n % 10 <=9) || (n % 100 >=11 && n % 100 <=14 )) ? 2: 3);
 ДОДАТИ ПІКТОГРАМУ!add:FBTN РОЗШИРЕНЕ!help-hint:FBTN Додати обрану піктограму програми Виберіть, що робити за допомогою піктограми $EXEC Двічі клацніть будь-яку програму, щоб перемістити її піктограму: Двічі клацніть будь-яку програму, щоб вилучити її піктограму: ДОПОМОГА!help:FBTN Довідка::TXT Перемістити Жодних змін не внесено!\nПОРАДА: ви завжди можете спробувати кнопку «Розширене». ВИЛУЧИТИ ПІКТОГРАМУ!remove:FBTN Видалити Цей скрипт призначений для запуску лише на стільниці IceWM СКАСУВАТИ ОСТАННЄ!undo:FBTN Попередження Попередження::TXT Ви використовуєте стільнмцю IceWM файл щось містить файл порожній нічого не вибрано 