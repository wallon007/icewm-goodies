��          �      ,      �     �     �     �  ?   �  5   �               5  #   ;  -   _  �   �       #   %  !   I     k     �  �  �     2     7  
   ?  K   J  =   �     �     �     �     �  :     w   N     �  &   �  (     !   0  #   R                                                    	   
                       Apply Back Change Names Click Apply when desired names of workspaces have been entered. Click Done when desired number of workspaces was set. Done Enter the desired names: Leave Set the desired number of desktops: To change Workspace names click Change Names. Use mouse wheel or click on arrows. You can also enter a number by keyboard followed by enter key. The change takes place immediately. Workspace Number aWCS antiX workspace count switcher aWCS antiX workspace name changer antiX workspace count switcher antiX workspace name changer Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-28 15:48+0000
Last-Translator: heskjestad <cato@heskjestad.xyz>, 2023
Language-Team: Norwegian Bokmål (https://www.transifex.com/anticapitalista/teams/10162/nb/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nb
Plural-Forms: nplurals=2; plural=(n != 1);
 Bruk Tilbake Endre navn Velg «Bruk» når de ønskede navnene på arbeidsområdene er skrevet inn. Velg «Ferdig» når ønsket antall arbeidsområder er valgt. Ferdig Skriv inn ønskede navn: Forlat Velg antall skrivebord: Velg «Endre navn» for å endre navn på arbeidsområder. Bruk musehjulet eller trykk på pilene, eller skriv inn et tall og trykk enter. Endringen vil tas i bruk med det samme. Nummer på arbeidsområde aWCS antiX Velg antall arbeidsområder aWCS antiX Endre navn på arbeidsområde antiX Velg antall arbeidsområder antiX Endre navn på arbeidsområde 