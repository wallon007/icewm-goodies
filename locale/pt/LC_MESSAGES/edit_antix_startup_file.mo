��          t      �                 (  -   D     r     �     �  	     ^     "   x     �  �  �     L  /   j  -   �  �   �     [     c     {  h   �  "   �  6                     
                                      	    Add a (manual) command Add application from a list Enter command to be added to the startup file Enter command you want to run at antiX's startup. Note: an ampersand/& will automatically be appended to the end of the command Remove Remove an application Startup ( The line was added to $startupfile. It will start automatically the next time you start  antiX \n $add_remove_text $startupfile): antiX-startup GUI Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-05-15 17:18+0000
Last-Translator: Paulo C., 2023
Language-Team: Portuguese (https://app.transifex.com/anticapitalista/teams/10162/pt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Adicionar um comando (manual) Adicionar uma aplicação a partir de uma lista Insira o comando a ser adicionado ao arranque  96% match 
Insira o comando que deseja executar no arranque do antiX. Nota: um 'e comercial &' será automaticamente adicionado ao fim do comando Remover Remover uma aplicação Ficheiro Startup ( A linha foi adicionada a $startupfile. O programa em causa iniciará automaticamente ao iniciar o  antiX \n $add_remove_text $startupfile): Adicionar ou remover aplicações ao arranque do antiX 