��          �      ,      �     �     �     �  ?   �  5   �               5  #   ;  -   _  �   �       #   %  !   I     k     �  _  �                 l   "  l   �     �  "        &  1   -  `   _  �   �     �  ,   �  2   �  '     -   .                                                    	   
                       Apply Back Change Names Click Apply when desired names of workspaces have been entered. Click Done when desired number of workspaces was set. Done Enter the desired names: Leave Set the desired number of desktops: To change Workspace names click Change Names. Use mouse wheel or click on arrows. You can also enter a number by keyboard followed by enter key. The change takes place immediately. Workspace Number aWCS antiX workspace count switcher aWCS antiX workspace name changer antiX workspace count switcher antiX workspace name changer Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-28 15:48+0000
Last-Translator: Green, 2023
Language-Team: Japanese (https://www.transifex.com/anticapitalista/teams/10162/ja/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ja
Plural-Forms: nplurals=1; plural=0;
 適用 戻る 名前変更 任意のワークスペース名称を入力したら、適用ボタンをクリックしてください。 希望するワークスペース数を設定したら、完了ボタンをクリックしてください。 完了 任意の名前を入力します: 終了 任意のデスクトップ数に設定します: ワークスペース名を変更するには、名前変更ボタンをクリックします。 マウスホイールまたは矢印をクリックしてください。キーボードで数字を入力しエンターキーを押しても機能します。変更はすぐ有効になります。 ワークスペースの番号 aWCS antiX ワークスペース数の切替 aWCS antiX ワークスペース名変更ツール antiX ワークスペース数の切替 antiX ワークスペース名変更ツール 