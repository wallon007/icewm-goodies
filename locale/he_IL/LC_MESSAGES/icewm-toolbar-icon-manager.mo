��          �   %   �      P     Q     c     {  (   �  !   �  .   �  0        >     M     l  	   q  
   {  D   �     �  G   �          .  7   5     m     �     �      �     �     �     �  �  �     �       )   !  =   K  3   �  F   �  H        M  &   `  
   �     �     �  e   �  
   $	  l   /	     �	     �	  [   �	  )   
  *   G
  
   r
  1   }
     �
     �
     �
                                                                            	         
                                        ADD ICON!add:FBTN ADVANCED!help-hint:FBTN Add selected app's icon Choose application to add to the Toolbar Choose what do to with $EXEC icon Double click any Application to move its icon: Double click any Application to remove its icon: HELP!help:FBTN MOVE ICON!gtk-go-back-rtl:FBTN Move Move left Move right No changes were made!\nTIP: you can always try the Advanced buttton. Ok Please select any option from the buttons below to manage Toolbar icons REMOVE ICON!remove:FBTN Remove This script is meant to be run only in an IceWM desktop Toolbar Icon Manager UNDO LAST STEP!undo:FBTN Warning You are running an IceWM desktop file has something file is empty nothing was selected Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-02-26 16:23+0000
Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>, 2023
Language-Team: Hebrew (Israel) (https://app.transifex.com/anticapitalista/teams/10162/he_IL/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: he_IL
Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n == 2 && n % 1 == 0) ? 1: (n % 10 == 0 && n % 1 == 0 && n > 10) ? 2 : 3;
 הוספת סמל!add:FBTN מתקדם!help-hint:FBTN הוספת סמל היישום הנבחר נא לבחור יישום להוספה לסרגל הכלים נא לבחור מה לעשות עם הסמל $EXEC לחיצה כפולה על יישום תעביר את הסמל שלו: לחיצה כפולה על יישום כלשהו תסיר את הסמל: עזרה!help:FBTN העברת סמל!gtk-go-back-rtl:FBTN העברה העברה שמאלה העברה ימינה לא בוצעו שינויים!\nעצה: תמיד אפשר לנסות את הכפתור המתקדם. אישור נא לבחור אפשרות מהכפתורים שלהלן כדי לנהל את סמלי סרגל הכלים הסרת סמל!remove:FBTN הסרה הסקריפט הזה מיועד להרצה בשולחן עבודה מסוג IceWM בלבד מנהל סמלים בסרגלי כלים ביטול הצעד האחרון!undo:FBTN אזהרה מופעל אצלך שולחן העבודה IceWM בקובץ יש משהו הקובץ ריק שום דבר לא נבחר 